[![BuildStatus](https://travis-ci.org/BankFacil/job_function.png)](https://travis-ci.org/BankFacil/job_function.png)
[![CodeClimate](https://codeclimate.com/github/BankFacil/job_function.png)](https://codeclimate.com/github/BankFacil/job_function)

# JobFunction

Gem que carrega uma lista de profissões

## Installation

Add this line to your application's Gemfile:

    gem 'job_function'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install job_function

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
