require "job_function/version"
require "job_function/jobs"


module ActionView
  module Helpers
    module FormOptionsHelper
      #
      # Return select and option tags
      # for the given object and method,
      # using job_functions_for_select to
      # generate the list of option tags.
      #
      def job_function(object, method, options = {},
                                       html_options = {})

        tag = if defined?(ActionView::Helpers::InstanceTag) &&
                ActionView::Helpers::InstanceTag.instance_method(:initialize).arity != 0

                InstanceTag.new(object, method, self, options.delete(:object))
              else
                JobFunction.new(object, method, self, options)
              end

        tag.to_job_function_tag(options, html_options)
      end

      def job_functions_for_select(selected = nil)
        values = ::JobFunction::LIST


        return options_for_select(values, selected)
      end

    end

    module ToJobFunctionTag
      def to_job_function_tag(options, html_options)
        html_options = html_options
        add_default_name_and_id(html_options)
        value = value(object)
        content_tag("select",
          add_options(
            job_functions_for_select(value),
            options, value
          ), html_options
        )
      end
    end

    if defined?(ActionView::Helpers::InstanceTag) &&
        ActionView::Helpers::InstanceTag.instance_method(:initialize).arity != 0
      class InstanceTag
        include ToJobFunctionTag
      end
    else
      class JobFunction < Tags::Base
        include ToJobFunctionTag
      end
    end

    class FormBuilder
      def job_function(method, options = {},
                                 html_options = {})

        @template.job_function(@object_name, method, options.merge(:object => @object),
                                                       html_options)
      end
    end
  end
end
