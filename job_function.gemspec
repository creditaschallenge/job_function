# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'job_function/version'

Gem::Specification.new do |s|
  s.name          = "job_function"
  s.version       = JobFunction::VERSION
  s.authors       = ["ruan brandao", "bruno costa"]
  s.email         = ["ruan.bernardo@gmail.com", "brunoadacosta@gmail.com"]
  s.description   = %q{Lista de cargos}
  s.summary       = %q{Carrega lista de cargos}
  s.homepage      = "https://github.com/BankFacil/job_function"
  s.license       = "MIT"

  s.files         = `git ls-files`.split($/)
  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename(f) }
  s.test_files    = s.files.grep(%r{^(test|spec|features)/})
  s.require_paths = ["lib"]

  s.add_development_dependency "bundler", "~> 1.3"
  s.add_development_dependency "rake"
  s.add_development_dependency "actionpack"
  s.add_development_dependency "rspec"
end
