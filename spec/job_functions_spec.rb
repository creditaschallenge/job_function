require 'spec_helper'

require 'action_view'
require 'job_function'

module ActionView
  module Helpers

    describe JobFunction do
      include TagHelper

      class Emprego
        attr_accessor :job_function_name
      end

      let(:emprego) { Emprego.new }

      let!(:template) { ActionView::Base.new }

      let(:builder) do
        if defined?(Tags::Base)
          FormBuilder.new(:emprego, emprego, template, {})
        else
          FormBuilder.new(:emprego, emprego, template, {}, Proc.new { })
        end
      end

      describe '#country_select' do
        it 'creates a select tag' do
          tag = builder.job_function(:job_function_name)
          expect(tag).to include("<select id=\"emprego_job_function_name\" name=\"emprego[job_function_name]\">")
        end

        it 'selected job' do
          emprego.job_function_name = 'Analista De Sistemas'
          tag = builder.job_function(:job_function_name)
          expect(tag).to include("value=\"16\">ANALISTA DE SISTEMAS</option>")
        end
      end
    end
  end
end
